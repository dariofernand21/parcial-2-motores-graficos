using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagement : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManagement instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.AudioSource = gameObject.AddComponent<AudioSource>();
            s.AudioSource.clip = s.SoundClip;
            s.AudioSource.volume = s.Volume;
            s.AudioSource.pitch = s.pitch;
            s.AudioSource.loop = s.repeat;
        }
    }

    public void SoundPlay(string name)
    {
        Sound s = System.Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + "not found.");
            return;
        }
        else
        {
            s.AudioSource.Play();
        }
    }

    public void SoundPause(string name)
    {
        Sound s = System.Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + "not found.");
            return;
        }
        else
        {
            s.AudioSource.Pause();
        }
    }

}
