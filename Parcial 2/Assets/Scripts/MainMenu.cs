using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public void GameScene()
	{
		SceneManager.LoadScene("Parcial 2");
	}

	public void Quit()
	{
		Application.Quit();
	}
	
}
