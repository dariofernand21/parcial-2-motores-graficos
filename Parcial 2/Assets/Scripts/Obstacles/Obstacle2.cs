using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle2 : MonoBehaviour
{
    public float moveSpeed = 6f;

    public float timeToLive = 5f;

    public float timeSinceSpawned = 0f;
               
       
    void Update()
    {
       
        transform.position += moveSpeed * Vector3.up * Time.deltaTime;             
        
        transform.LookAt(Vector3.forward);
                
        timeSinceSpawned += Time.deltaTime;

        
        if(timeSinceSpawned > timeToLive)
        {
            Destroy(gameObject);
        }
    }

}
