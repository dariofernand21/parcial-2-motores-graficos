using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;

    public GameObject aimUI;

    public GameObject optionsUI;        

    public KeyCode swingKey = KeyCode.Mouse0;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //visiblidad del cursor
        Cursor.visible = false;
       
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }

            optionsUI.SetActive(false);
        }

        if (Input.GetKeyDown(swingKey) && GameIsPaused == false)
        {
            AudioManagement.instance.SoundPlay("GrapplingHook");
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        aimUI.SetActive(true);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        aimUI.SetActive(false);
        Time.timeScale = 0f;
        GameIsPaused = true;
        Cursor.lockState = CursorLockMode.None;
        
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Main Menu");
        AudioManagement.instance.SoundPause("Victory");
        AudioManagement.instance.SoundPause("Alarm");
        AudioManagement.instance.SoundPause("AcidActivation");
    }

    public void QuitGame()
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }

    public void OptionsMenu()
    {
        Debug.Log("Loading options...");
        optionsUI.SetActive(true);
        
    }

    public void OptionsBackButton()
    {
        optionsUI.SetActive(false);
    }

    public void RestartButton()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
        AudioManagement.instance.SoundPause("Victory");
    }
}
