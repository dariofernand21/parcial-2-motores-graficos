using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    
    [Header("Movement")]
    private float moveSpeed;
    public float walkSpeed;
    public float sprintSpeed;
    public float swingSpeed;

    public float groundDrag;
    

    [Header("Jumping")]
    public float jumpForce;
    public float jumpCooldown;
    public float airMultiplier;
    bool readyToJump;

    [Header("Keybinds")]
    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode sprintKey = KeyCode.LeftShift;

    [Header("Ground Check")]
    public float playerHeight;
    public LayerMask WhatIsGround;
    bool grounded;

    [Header("Slope Handling")]
    public float maxSlopeAngle;
    private RaycastHit slopeHit;
    private bool exitingSlope;
  
    [Header("Flashlight")]
    public bool isOn = false;
    public GameObject lightSource;
    public bool failSafe = false;
    
    public GameObject gameOverUI;
    
    public Transform orientation;

    float horizontalInput;
    float verticalInput;

    Vector3 moveDirection;

    [Header("Acid Event")]
    public GameObject escapeText;
    public GameObject acidText;
    public bool acidRise;
    public GameObject acidTrigger;
    public GameObject acidCube;
    public float acidSpeed;

    public GameObject secretDoor; 

    [Header("Barrier Event")]
    public GameObject smokeBarrier;
    public GameObject smokeText;
    public bool smokeMoves;
    public float smokeSpeed;
    public GameObject smokeCube;
    public GameObject acidBalls;   
        

    Rigidbody rb;
        
    public MovementState state;

    public enum MovementState
    {
        swinging,
        grappling,
        freeze,
        walking,
        sprinting,
        air
    }

    public bool freeze;
    public bool activeGrapple;
    public bool swinging;
    
    private void Start() //VOID STARD
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;

        readyToJump = true;
        acidText.SetActive(false);
        escapeText.SetActive(false);

       
    }

    IEnumerator WaitForSec()
    {
        yield return new WaitForSeconds(5);
        Destroy(acidText);            
        
    }

    IEnumerator WaitForSmoke()
    {
        yield return new WaitForSeconds(5);
        Destroy(smokeText);
    }

    IEnumerator WaitForEscape()
    {
        yield return new WaitForSeconds(5);
        Destroy(escapeText);
    }

    void Update() //VOID UPDATE
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, WhatIsGround); //ground check
        
        MyInput();
        SpeedControl();
        StateHandler();
        
        /*if (Input.GetButtonDown("EKey"))
        {
            theLever.GetComponent<Animator>().Play("Lever");
        }*/

        if (grounded && !activeGrapple) //drag control
        {
            rb.drag = groundDrag;
        }
        
        else
        {    
        rb.drag = 0;
        }  
        
        if (Input.GetKeyDown(jumpKey) && grounded)
        {
            AudioManagement.instance.SoundPlay("Jump");
        }


        if (Input.GetKeyDown("r"))
        {
            transform.position = new Vector3(-1, 1, 0);
            AudioManagement.instance.SoundPlay("Respawn");
            
        
            /*Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);*/
        }

        if (acidRise == true)
        {
            acidCube.transform.Translate(Vector3.up * acidSpeed * Time.deltaTime);
        }

        if (smokeMoves == true)
        {            
            smokeCube.transform.Translate(Vector3.forward * smokeSpeed * Time.deltaTime);                    
        }
             
        if (Input.GetButtonDown("FKey"))
        {
            if (isOn == false && failSafe == false)
            {
                AudioManagement.instance.SoundPlay("FON");
                failSafe = true;
                lightSource.SetActive(true);
                isOn = true;                
                StartCoroutine(FailSafe());
                
            }
            if (isOn == true == failSafe == false)
            {
                AudioManagement.instance.SoundPlay("FOFF");
                failSafe = true;
                lightSource.SetActive(false);
                isOn = false;                
                StartCoroutine(FailSafe());
            }
        }            

    }

    IEnumerator FailSafe()
    {
        yield return new WaitForSeconds(0.25f);
        failSafe = false;
    }
  
    
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Respawn")
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
            AudioManagement.instance.SoundPlay("Respawn");
            AudioManagement.instance.SoundPause("Victory");
        }

         if (other.tag == "Obstacle")
        {
            gameOverUI.SetActive(true);
            Time.timeScale = 0f;
            Cursor.lockState = CursorLockMode.None;
            Destroy(secretDoor);
            AudioManagement.instance.SoundPause("Alarm");
            AudioManagement.instance.SoundPause("AcidActivation");
            
        }
        /*

        if (other.tag == "Checkpoint 0")
        {
            transform.position = new Vector3(-1, 1, 0);
            AudioManagement.instance.SoundPlay("Respawn");
        }
          
        if (other.tag == "Checkpoint 1")
        {
            transform.position = new Vector3(33, 3, 52);
            AudioManagement.instance.SoundPlay("Respawn");
        }

        if (other.tag == "Checkpoint 2")
        {
            transform.position = new Vector3(86, 1, 52);
            AudioManagement.instance.SoundPlay("Respawn");
        }

        if (other.tag == "Checkpoint 3")
        {
            transform.position = new Vector3(195, 1, 68);
            AudioManagement.instance.SoundPlay("Respawn");
        } */

        if (other.tag == "TriggerAcid")
        {
            acidRise = true;
            AudioManagement.instance.SoundPlay("AcidActivation");
            acidText.SetActive(true);
            StartCoroutine("WaitForSec");
        }

        if (other.tag == "Gas")
        {
            smokeMoves = true;
            AudioManagement.instance.SoundPlay("Alarm");
            smokeText.SetActive(true);
            StartCoroutine("WaitForSmoke");
        }
        
        if (other.tag == "Finish")
        {
            escapeText.SetActive(true);
            StartCoroutine("WaitForEscape");
            Destroy(secretDoor);           
            smokeSpeed = 0f;
            acidSpeed = 0f;
            AudioManagement.instance.SoundPlay("SDoor");
            AudioManagement.instance.SoundPlay("Chill");
            AudioManagement.instance.SoundPause("Alarm");
            AudioManagement.instance.SoundPause("AcidActivation");
        }

        if (other.tag == "AcidDespawn")
        {
             
            Destroy(acidBalls);

        }
               
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void MyInput()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");

        if(Input.GetKey(jumpKey) && readyToJump && grounded)
        {
            readyToJump = false;

            Jump();

            Invoke(nameof(ResetJump), jumpCooldown);
        }
    }

    private void StateHandler()
    {
        if (freeze)
        {
            state = MovementState.freeze;
            moveSpeed = 0;
            rb.velocity = Vector3.zero;
        }
        
        else if (activeGrapple)
        {
            state = MovementState.grappling;
            moveSpeed = sprintSpeed;
        }

        else if (swinging)
        {
            state = MovementState.swinging;
            moveSpeed = swingSpeed;
        }

        else if (grounded && Input.GetKey(sprintKey))
        {
            state = MovementState.sprinting;
            moveSpeed = sprintSpeed;
        }

        else if (grounded)
        {
        state = MovementState.walking;
        moveSpeed = walkSpeed;
        }

        else 
        {
            state = MovementState.air;
        }
    }

    private void MovePlayer()
    {
        if(activeGrapple) return;
        if (swinging) return;        

        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput; //orientacion de movimiento hacia adelante

        if(OnSlope() && !exitingSlope)
        {
            rb.AddForce(GetSlopeMoveDirection() * moveSpeed * 20f, ForceMode.Force);

            if (rb.velocity.y > 0)
                rb.AddForce(Vector3.down * 80f, ForceMode.Force);
        }

        else if(grounded)
         
            rb.AddForce(moveDirection.normalized * moveSpeed * 10f, ForceMode.Force);
        
        else if(!grounded)
            rb.AddForce(moveDirection.normalized * moveSpeed * 10f * airMultiplier, ForceMode.Force);

        
        rb.useGravity = !OnSlope();     

    }   

    private void SpeedControl()
    {
        if(activeGrapple) return;

        if(OnSlope() && !exitingSlope)
        {
            if(rb.velocity.magnitude > moveSpeed)
                rb.velocity = rb.velocity.normalized * moveSpeed;
        }

        else
        {
            Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

            if (flatVel.magnitude > moveSpeed) //limitar velocidad
            {
                Vector3 limitedVel = flatVel.normalized * moveSpeed;
                rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
            }
        }
        

    }

    private void Jump()
    {
        exitingSlope = true;
        
        rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }

    private void ResetJump()
    {
        readyToJump = true;

        exitingSlope = false;
    }

    private bool enableMovementOnNextTouch;

    public void JumpToPosition(Vector3 targetPosition, float trajectoryHeight)
    {
        activeGrapple = true;

        velocityToSet = CalculateJumpVelocity(transform.position, targetPosition, trajectoryHeight);
        Invoke(nameof(SetVelocity), 0.1f);

        Invoke(nameof(ResetRestrictions), 3f);
    }

    private Vector3 velocityToSet;

    private void SetVelocity()
    {
        enableMovementOnNextTouch = true;
        rb.velocity = velocityToSet;
    }

    public void ResetRestrictions()
    {
        activeGrapple = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(enableMovementOnNextTouch)
        {
            enableMovementOnNextTouch = false;
            ResetRestrictions();

            GetComponent<Grappling>().StopGrapple();
        }
    }

    private bool OnSlope()
    {
        if(Physics.Raycast(transform.position, Vector3.down, out slopeHit, playerHeight * 0.5f + 0.3f))
        {
            float angle = Vector3.Angle(Vector3.up, slopeHit.normal);
            return angle < maxSlopeAngle && angle != 0;
        }

        return false;
    }

    private Vector3 GetSlopeMoveDirection()
    {
        return Vector3.ProjectOnPlane(moveDirection, slopeHit.normal).normalized;
    }

    public Vector3 CalculateJumpVelocity(Vector3 startPoint, Vector3 endPoint, float trajectoryHeight)
    {
        float gravity = Physics.gravity.y;
        float displacementY = endPoint.y - startPoint.y;
        Vector3 displacementXZ = new Vector3(endPoint.x - startPoint.x, 0f, endPoint.z - startPoint.z);

        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * trajectoryHeight);
        Vector3 velocityXZ = displacementXZ / (Mathf.Sqrt(-2 * trajectoryHeight / gravity)
            + Mathf.Sqrt(2 * (displacementY - trajectoryHeight) / gravity));

            return velocityXZ + velocityY;
    }
}
