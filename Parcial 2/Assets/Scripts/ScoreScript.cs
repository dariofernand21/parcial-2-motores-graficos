using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text CollectedText;
    private int ScoreNum;
    

   
    void Start()
    {
        ScoreNum = 0;
        CollectedText. text = "" + ScoreNum;
    }

    private void OnTriggerEnter(Collider Collectible)    
    {    
        if (Collectible.tag == "Collectible")
        {

            ScoreNum += 1;
            Destroy(Collectible.gameObject);
            CollectedText.text = "" + ScoreNum;
            AudioManagement.instance.SoundPlay("Collect");
        }
    }
}
