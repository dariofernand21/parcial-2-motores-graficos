using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SecretInteraction : Interactable
{
	public GameObject hiddenPassage; 
	
	public GameObject interactText;

   void Start()
   {
		 hiddenPassage.SetActive(false); 
   }

   public override void OnFocus()
   {
		interactText.SetActive(true);
   }

   public override void OnInteract()
   {
		 AudioManagement.instance.SoundPlay("Button");
		 hiddenPassage.SetActive(true);

   }

   public override void OnLoseFocus()
   {
		 interactText.SetActive(false);
   }
}
