using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SecretInteraction2 : Interactable
{
	public GameObject hiddenAlien1;
	public GameObject hiddenAlien2;
	
	public GameObject interactText;

   void Start()
   {
		 hiddenAlien1.SetActive(false); 
		 hiddenAlien2.SetActive(false); 
   }

   public override void OnFocus()
   {
		interactText.SetActive(true);
   }

   public override void OnInteract()
   {
		 AudioManagement.instance.SoundPlay("Button");
		 hiddenAlien1.SetActive(true); 
		 hiddenAlien2.SetActive(true); 

   }

   public override void OnLoseFocus()
   {
		 interactText.SetActive(false);
   }
}

