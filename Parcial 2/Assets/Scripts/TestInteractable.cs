using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class TestInteractable : Interactable
{
   public GameObject testText;

   public GameObject victoryUI;
   
   

   public override void OnFocus()
   {
		testText.SetActive(true);
   }

   public override void OnInteract()
   {
		 AudioManagement.instance.SoundPlay("Button");
		 victoryUI.SetActive(true);
		 Time.timeScale = 0f;
		 Cursor.lockState = CursorLockMode.Locked;
		 AudioManagement.instance.SoundPause("Chill");
		 AudioManagement.instance.SoundPlay("Victory");
   }

   public override void OnLoseFocus()
   {
		 testText.SetActive(false);
   }
}
